import time
import win32com.client
import win32api
import win32event
import json
import pythoncom
from win32com.client import constants
from wait import WaitWhileProcessingMessages
from connection import connection
class Lector():
    global relojOCX
    def __init__(self):
        print('init lector')
    def setLector(self, lector):
        self.ocx = lector
        self.cache = -1
    def initEngine(self):
        error = self.ocx.InitEngine()
        print('InitEngine: %d' % error)
        return {'cmd':'initEngine', 'data':{'ready': error == 0}}
    def ready(self):
        return {'cmd':'ready', 'data':{'ready': self.ocx.Active}}
    def enroll(self, socio_id, server):
        self.socio_id = socio_id
        self.ocx.InitEngine()
        # self.ocx.CancelCapture()
        self.ocx.EnrollCount = 3
        lector.ocx.ControlSensor(11, 1)
        lector.ocx.ControlSensor(11, 0)
        print(self.ocx.IsRegister)
        print(self.ocx.EngineValid)
        print(self.ocx.EnrollIndex)
        self.ocx.BeginEnroll()
        print(self.ocx.IsRegister)
        self.enrollResult = -1
        print('begin enroll...')
        if not WaitWhileProcessingMessages(self.ocx.event):
            print("Error enrolling (WaitWhileProcessingMessages)")
        else:
            print('ok enrolling (WaitWhileProcessingMessages)')
        return {'cmd':'enroll', 'data':{'status': self.enrollResult}}
    def enrollFinish(self, ActionResult, template):
        self.ocx.CancelEnroll()  
        if (ActionResult):
            print("Registro completo, voy a grabar")
            self.enrollSave(template)
            #self.refreshData()
        else:
            print("Error de registro")
        #return {'cmd':'enroll', 'data':{'status': ActionResult}}
    def enrollSave(self, template):
        tpl = self.ocx.GetTemplateAsString()
        print(lector.socio_id)
        print(tpl)
        sql = "UPDATE socio SET template = '%s' WHERE id = %d" % (tpl, lector.socio_id)
        try:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()
            if(self.cache == -1):
                 self.refreshData()
            add = self.ocx.AddRegTemplateStrToFPCacheDB(self.cache, lector.socio_id, tpl)
            print('add %d' % add )            
        except:
           print('error')
           connection.rollback()
        print('add %d' % add )           
    def deleteTemplates(self):
        tpl = self.ocx.GetTemplateAsString()
        sql = "UPDATE socio SET template = null where template is not null"
        try:
           cursor = connection.cursor()
           cursor.execute(sql)
           connection.commit()
           self.ocx.FreeFPCacheDB(self.cache)
           self.cache = -1
           print('Deleted')
        except:
           print('error')
           connection.rollback()      
    def refreshData(self):
        if(self.cache != -1):
          self.ocx.FreeFPCacheDB(self.cache)
        self.cache = self.ocx.CreateFPCacheDB()
        cursor = connection.cursor()
        sql = "SELECT id, template from socio where template is not null"
        try:
           cursor.execute(sql)
           results = cursor.fetchall()
           for row in results:
              id = row[0]
              template = row[1]
              print(self.cache)
              print('id %d' % id)
              add = self.ocx.AddRegTemplateStrToFPCacheDB(self.cache, id, template)
              print('add %d' % add )
        except:
            print('error')
            print(sys.exc_info())
            print (sys.exc_info()[0])
    def check(self):
        print(self.cache)
        if self.cache == -1:
          self.refreshData()
        self.captureId = -1;
        self.ocx.EnrollCount = 1
        self.ocx.CancelEnroll()        
        self.ocx.CancelCapture()
        self.ocx.BeginCapture()
        if not WaitWhileProcessingMessages(self.ocx.event, 10):
            self.captureId = (-2,0,0) #para que el cliente sepa que es un timeout
            print("Error BeginCapture (WaitWhileProcessingMessages)")
        else:
            print('ok BeginCapture (WaitWhileProcessingMessages)')
        return {'cmd':'check', 'data':{'resultado': self.captureId}}
    def captureFinish(self, ActionResult, template):
        print('OnCapture ActionResult %d' % ActionResult)
        print('capture finished')
        ProcessNum = 0
        Score = 3
        self.captureId = self.ocx.IdentificationInFPCacheDB(lector.cache, template, Score, ProcessNum);
        if self.captureId[0] == -1:
          lector.ocx.ControlSensor(12, 1)
          lector.ocx.ControlSensor(12, 0)
        else:
          lector.ocx.ControlSensor(11, 1)
          lector.ocx.ControlSensor(11, 0)
        print(self.captureId)
    def stop(self):
        lector.ocx.ControlSensor(12, 1)
        lector.ocx.ControlSensor(12, 0)
        print('trying to stop')
        win32event.SetEvent(win32event.CreateEvent(None, 0, 0, None))
        return {'cmd':'stop', 'data':{}}
lector = Lector()
