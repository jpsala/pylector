import win32com.client
import win32api
import win32event
import pythoncom
from lector import lector
from win32com.client import constants
defaultNamedNotOptArg = pythoncom.Empty
global lector
class OCXEvents:
    def __init__(self):
        self.event = win32event.CreateEvent(None, 0, 0, None)
        lector.ocxEvent = self.event
        print("Init en OCXEvents")
    def OnEnroll(self, ActionResult=defaultNamedNotOptArg, ATemplate=defaultNamedNotOptArg):
        print('OnEnroll ActionResult %d' % ActionResult)
        thread = win32api.GetCurrentThreadId()
        lector.enrollResult = ActionResult
        lector.enrollFinish(ActionResult, ATemplate)
        win32event.SetEvent(self.event)
    def OnCapture(self, ActionResult=defaultNamedNotOptArg, ATemplate=defaultNamedNotOptArg):
        #lector.ocx.ControlSensor(13, 1)
        thread = win32api.GetCurrentThreadId()
        lector.captureFinish(ActionResult, ATemplate)
        win32event.SetEvent(self.event)
    def OnFeatureInfo(self, AQuality=defaultNamedNotOptArg):
        thread = win32api.GetCurrentThreadId()
        print('OnFeatureInfo')
        print(AQuality)
    def OnImageReceived(self, AImageValid=defaultNamedNotOptArg):        
        self.beep();
        lector.ocx.ControlSensor(11, 1)
        lector.ocx.ControlSensor(11, 0)
        if AImageValid:
            print('ok')
        else:
            print('error')
    def OnFingerTouching(self):
        thread = win32api.GetCurrentThreadId()
        print('OnFingerTouching')
    def OnFingerLeaving(self):
        thread = win32api.GetCurrentThreadId()
        print('OnFingerLeaving')
    def beep(self):
        lector.ocx.ControlSensor(13, 1)
        lector.ocx.ControlSensor(13, 0)
