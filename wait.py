import time
import win32event
import pythoncom
def WaitWhileProcessingMessages(event, timeout = 20):
    start = time.clock()
    while True:
        # Wake 4 times a second - we can't just specify the
        # full timeout here, as then it would reset for every
        # message we process.
        rc = win32event.MsgWaitForMultipleObjects( (event,), 0,
            250,
            win32event.QS_ALLEVENTS)
        if rc == win32event.WAIT_OBJECT_0:
            # event signalled - stop now!
            return True
        if (time.clock() - start) > timeout:
            # Timeout expired.
            return False
        # must be a message.
        pythoncom.PumpWaitingMessages()
