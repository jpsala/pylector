from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
from lector import lector
import win32com.client
import win32api
import win32event
import json
import pythoncom
from win32com.client import constants
defaultNamedNotOptArg = pythoncom.Empty
import pymysql
from ocxEvents import OCXEvents
from connection import connection

global ocx

class HandleSocket(WebSocket):
    def handleMessage(self):
        msg = json.loads(self.data)
        print(msg)
        cmd = msg['cmd']
        if(cmd == 'echo'):
            resp = msg
        elif(cmd == 'initEngine'):
            resp = lector.initEngine()
        elif(cmd == 'ready'):
            resp = lector.ready()
        elif(cmd == 'enroll'):
            print(msg['data']['socio_id'])
            resp = lector.enroll(msg['data']['socio_id'], self)
        elif(cmd == 'refreshData'):
            resp = lector.refreshData()
        elif(cmd == 'deleteTemplates'):
            resp = lector.deleteTemplates()
        elif(cmd == 'check'):
            resp = lector.check()
        elif(cmd == 'stop'):
            resp = lector.stop()
        else:
            resp = msg

        self.sendMessage(json.dumps(resp))

    def handleConnected(self):
        print(self.address, 'connected')

    def handleClose(self):
        print(self.address, 'closed')

ocx = win32com.client.DispatchWithEvents("ZKFPEngXControl.ZKFPEngX", OCXEvents)
lector.setLector(ocx)
server = SimpleWebSocketServer('', 8000, HandleSocket)
ocx.server = server
lector.server = server
lector.ocx = ocx
server.serveforever()
